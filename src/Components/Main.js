import styles from "../Styles/Main.module.css";
import { PropTypes } from "prop-types";

export default function Main(props) {

  return (
    <ul className={styles.main}>
      {props.children.map((item) => (
        <li className={styles.main__item}  key={item.id}>
            <h2> {item.title}</h2>
            <img className={styles.main__item_img} src={item.url}  alt={"imagen  :"+item.id}/>
             </li>
      ))}
    </ul>
  );
}


Main.propTypes = {
    data : PropTypes.array
}