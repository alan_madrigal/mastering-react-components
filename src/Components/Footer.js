import styles from "../Styles/Footer.module.css";
import { PropTypes } from "prop-types";

export default function Footer({title = "Footer title"}){

    return( 
        <div className={styles.footer}>
             <h1>{title}</h1>
        </div>
       
    )

}


Footer.propTypes = {
    title : PropTypes.string
}