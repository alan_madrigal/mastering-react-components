import styles from "../Styles/Header.module.css";
import { PropTypes } from "prop-types";

export default function Header({ title = "Header title" }) {
  return (
    <div className={styles.header}>
      <h1>{title} </h1>
    </div>
  );
}

Header.propTypes = {
  title: PropTypes.string,
};
