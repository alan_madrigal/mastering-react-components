
import styles from './App.module.css';
import Main from './Components/Main';
import Footer from './Components/Footer';
import Header from './Components/Header';
import { data } from './data';

function App() {
  return (
    <>
     <div className={styles.container}>

<Header  title="Title "/>
<Main >{data}</Main>
<Footer title="Footer"/>
</div>
</>
  
  );
}

export default App;
